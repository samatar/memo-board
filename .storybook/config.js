import React from "react";
import { configure, addDecorator } from "@storybook/react";
import GlobalStyles from "../client/styles/GlobalStyles";

function requireAll(requireContext) {
  return requireContext.keys().map(requireContext);
}

function loadStories() {
  requireAll(require.context("../client", true, /.story.js$/));
}

addDecorator(story => (
  <>
    <GlobalStyles />
    {story()}
  </>
));

configure(loadStories, module);
