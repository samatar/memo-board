const path = require("path");
const prod = process.env.NODE_ENV === "production";
const { HotModuleReplacementPlugin } = require("webpack");

module.exports = {
  mode: prod ? "production" : "development",
  watch: !prod,
  devtool: "sourcemap",
  target: "web",
  entry: [path.resolve(__dirname, "..", "..", "client", "entry.browser.js")],
  output: {
    path: path.resolve(__dirname, "..", "..", "assets"),
    filename: "js/[name].js",
    publicPath: "/assets",
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      },
      {
        test: /\.svg$/,
        loader: "svg-sprite-loader",
      },
    ],
  },
};

if (!prod) {
  module.exports.plugins = [new HotModuleReplacementPlugin()];
  module.exports.entry.unshift("webpack-hot-middleware/client");
}
