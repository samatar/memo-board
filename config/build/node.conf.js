const path = require("path");
const prod = process.env.NODE_ENV === "production";

module.exports = {
  mode: prod ? "production" : "development",
  watch: !prod,
  target: "node",
  entry: path.resolve(__dirname, "..", "..", "client", "entry.node.js"),
  output: {
    path: path.resolve(__dirname, "..", "..", "bundles"),
    filename: "[name].js",
    libraryTarget: "commonjs",
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /node_modules/,
        use: "babel-loader",
      },
      {
        test: /\.svg$/,
        loader: "svg-sprite-loader",
      },
    ],
  },
};
