import { createGlobalStyle } from "styled-components";

export const colors = {
  bg: "hsl(210, 30%, 95%)",
  black: "#444",
  white: "#fff",
};

const GlobalStyles = createGlobalStyle`
html {
  height: 100%;
  box-sizing: border-box;
}

*,
*::before,
*::after {
  box-sizing: inherit
}

body {
  height:100%;
  margin: 0;
  padding: 0;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
  font-size: 16px;
  color: ${colors.black};
  background-color: ${colors.bg};
  -webkit-font-smoothing: antialiased;
}

#root {
  height: 100%;
}

h1, h2, h3, h4, h5, h6, p {
  margin: 0;
  padding: 0;
}

ul {
  padding: 0;
  margin: 0;
  list-style-type: none; 
}

a {
  text-decoration: none; 
  color: inherit; 
}

button {
  cursor: pointer;
}
`;

export default GlobalStyles;
