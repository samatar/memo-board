import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "./app";
import configureStore from "./state/store";

const initialState = window.__STATE__ || {};

ReactDOM.hydrate(
  <Provider store={configureStore(initialState)}>
    <App />
  </Provider>,
  document.getElementById("root")
);
