import React from "react";
import PropType from "prop-types";
import { connect } from "react-redux";
import { Container, Heading, AddButton } from "./index.styles";
import { getNewIdea } from "../../state/actions";

class Header extends React.Component {
  static propTypes = {
    getNewIdea: PropType.func,
  };

  render() {
    return (
      <Container>
        <Heading>Memo Board</Heading>
        <AddButton onClick={this.props.getNewIdea}>Add idea!</AddButton>
      </Container>
    );
  }
}

export default connect(
  null,
  { getNewIdea }
)(Header);
