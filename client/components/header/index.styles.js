import styled from "styled-components";
import { colors } from "../../styles/GlobalStyles";
import { darken } from "polished";

export const Container = styled.header`
  display: flex;
  justify-content: space-between;
  background: ${darken(0.3, colors.bg)};
  padding: 40px 10px;
`;

export const Heading = styled.h1``;

export const AddButton = styled.button`
  font-size: 18px;
  background-color: ${colors.bg};
  border: none;
  border-radius: 2px;
  padding: 10px 15px;
  transition: background-color 0.2s;
  &:hover,
  &:focus {
    background-color: ${darken(0.1, colors.bg)};
  }
`;
