import React from "react";
import styled from "styled-components";

function Icon({ glyph, className, ...rest }) {
  return (
    <svg className={className} {...rest}>
      <use xlinkHref={`#${glyph.id}`} />
    </svg>
  );
}

const IconStyled = styled(Icon)`
  width: 24px;
  height: 24px;
`;

export default IconStyled;
