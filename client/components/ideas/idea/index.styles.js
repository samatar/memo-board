import styled from "styled-components";
import { colors } from "../../../styles/GlobalStyles";

export const Container = styled.li`
  position: relative;
  width: 150px;
  height: 150px;
  padding: 4px;
  background-color: ${colors.white};
  box-shadow: 1px 2px 4px rgba(0, 0, 0, 0.1);
`;

export const Form = styled.form`
  display: flex;
  height: 100%;
  flex-direction: column;
  justify-content: space-around;
`;

export const Title = styled.input`
  color: inherit;
  font-family: inherit;
  height: 20%;
  padding: 0;
  border: none;
  text-align: center;
  font-size: 16px;
  font-weight: bold;
  &:focus {
    outline: 2px solid lightgray;
  }
`;

export const Body = styled.textarea`
  color: inherit;
  font-family: inherit;
  padding: 0;
  font-size: 14px;
  height: 100%;
  border: none;
  resize: none;
  &:focus {
    outline: 2px solid lightgray;
  }
`;

export const DeleteButton = styled.button`
  position: absolute;
  z-index: 1;
  right: 0;
  top: 0;
  display: ${({ mobile }) => (mobile ? "block" : "none")};
  padding: 0;
  border: none;
  background: none;
  ${Container}:hover & {
    display: ${({ editing }) => (editing ? "none" : "block")};
  }
`;

export const CharacterCounter = styled.div`
  position: absolute;
  z-index: 1;
  display: ${({ showCharacterCount }) =>
    showCharacterCount ? "block" : "none"};
  top: 100%;
  left: 0;
  font-size: 14px;
  border-radius: 2px;
  padding: 2px;
  background: ${colors.black};
  color: ${colors.white};
`;
