import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateIdea, deleteIdea } from "../../../state/actions";
import {
  Container,
  Form,
  Title,
  Body,
  DeleteButton,
  CharacterCounter,
} from "./index.styles";
import Icon from "../../icon";
import Delete from "../../../styles/svg/delete.svg";

class Idea extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      titleVal: props.title,
      bodyVal: props.body,
      characterCount: props.body.length,
      editing: false,
      editingBody: false,
    };
    this.titleRef = React.createRef();
  }

  handleFocus = ev => {
    this.setState({ editing: true });
    if (ev.target.name === "idea_body") this.setState({ editingBody: true });
  };

  handleTitleChange = ev => {
    this.setState({ titleVal: ev.target.value });
  };

  handleBodyChange = ev => {
    const bodyVal =
      ev.target.value.length > 140
        ? ev.target.value.slice(0, 140)
        : ev.target.value;

    this.setState({
      bodyVal,
      characterCount: bodyVal.length,
    });
  };

  handleIdeaUpdate = () => {
    this.setState({ editing: false, editingBody: false });
    this.props.updateIdea({
      id: this.props.id,
      title: this.state.titleVal,
      body: this.state.bodyVal,
    });
  };

  handleIdeaDelete = () => {
    this.props.deleteIdea(this.props.id);
  };

  render() {
    const { characterCount, editingBody } = this.state;
    return (
      <Container>
        <Form>
          <DeleteButton
            mobile={this.props.mobile}
            aria-label="Delete Idea"
            onClick={this.handleIdeaDelete}
            editing={this.state.editing}
            type="button"
          >
            <Icon aria-hidden={true} glyph={Delete} />
          </DeleteButton>
          <Title
            name="idea_title"
            autoComplete="off"
            aria-label={"Enter the title of your idea"}
            ref={this.titleRef}
            placeholder="Title"
            onChange={this.handleTitleChange}
            onBlur={this.handleIdeaUpdate}
            onFocus={this.handleFocus}
            value={this.state.titleVal}
          />
          <Body
            name="idea_body"
            aria-label={"Enter the description of your idea"}
            placeholder="Enter your idea!"
            onChange={this.handleBodyChange}
            onBlur={this.handleIdeaUpdate}
            onFocus={this.handleFocus}
            value={this.state.bodyVal}
          />
        </Form>
        <CharacterCounter
          showCharacterCount={editingBody && characterCount > 125}
        >
          {140 - characterCount}
        </CharacterCounter>
      </Container>
    );
  }
}

Idea.propTypes = {
  title: PropTypes.string,
  body: PropTypes.string,
  id: PropTypes.string,
  updateIdea: PropTypes.func,
  deleteIdea: PropTypes.func,
  mobile: PropTypes.bool,
};

export default connect(
  null,
  { updateIdea, deleteIdea },
  null,
  { forwardRef: true }
)(Idea);
