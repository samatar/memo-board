import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Container } from "./index.styles";
import Idea from "./idea";
import { getIdeas } from "../../state/actions";

class Ideas extends React.Component {
  static propTypes = {
    getIdeas: PropTypes.func,
    ideas: PropTypes.array,
  };

  constructor(props) {
    super(props);
    this.refsCollection = {};
    this.mobile = null;
  }

  componentDidMount() {
    this.mobile = "ontouchstart" in document.documentElement;
    this.props.getIdeas();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.ideas.length < this.props.ideas.length) {
      const idea = this.refsCollection[
        this.props.ideas[this.props.ideas.length - 1].id
      ];
      idea.titleRef.current.focus();
    }
  }

  render() {
    return (
      <Container>
        {this.props.ideas.map(idea => (
          <Idea
            ref={instance => {
              this.refsCollection[idea.id] = instance;
            }}
            key={idea.id}
            mobile={this.mobile}
            {...idea}
          />
        ))}
      </Container>
    );
  }
}

const mapStateToProps = state => ({ ideas: state.ideas });

export default connect(
  mapStateToProps,
  { getIdeas }
)(Ideas);
