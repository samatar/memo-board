import React from "react";
import { storiesOf } from "@storybook/react";
import Idea from "../idea";

const props = {
  title: "My idea",
  body:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad mini.",
};

storiesOf("Idea", module).add("Default", () => <Idea {...props} />);
