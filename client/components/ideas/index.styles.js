import styled from "styled-components";

export const Container = styled.ul`
  display: grid;
  grid-template-columns: repeat(auto-fill, 150px);
  justify-content: space-around;
  grid-gap: 10px;
  padding: 20px 10px;
`;
