import { hot } from "react-hot-loader/root";
import React from "react";
import GlobalStyles from "./styles/GlobalStyles";
import Header from "./components/header";
import Ideas from "./components/ideas/";

function App() {
  return (
    <>
      <GlobalStyles />
      <Header />
      <Ideas />
    </>
  );
}

export default hot(App);
