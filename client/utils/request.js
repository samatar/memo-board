const fetch = require("isomorphic-fetch");

function request(url, options) {
  return fetch(url, options)
    .then(res => {
      if (res.status >= 200 && res.status < 300) {
        return res;
      } else {
        throw new Error(res.statusText);
      }
    })
    .then(res => {
      if (res.status === 204 || res.status === 205) {
        return null;
      }

      return res.json();
    });
}

module.exports = request;
