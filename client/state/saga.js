import { takeLatest, put, call } from "redux-saga/effects";
import request from "../utils/request";

import { GET_IDEAS, GET_NEW_IDEA, UPDATE_IDEA, DELETE_IDEA } from "./constants";
import {
  getIdeasSuccess,
  getIdeasError,
  getNewIdeaSuccess,
  getNewIdeaError,
  updateIdeaSuccess,
  updateIdeaError,
  deleteIdeaSuccess,
  deleteIdeaError,
} from "./actions";

//Get Ideas saga
export function* getIdeasSaga() {
  try {
    const response = yield call(request, "/ideas", {
      headers: {
        Accept: "application/json",
      },
    });
    yield put(getIdeasSuccess(response));
  } catch (err) {
    yield put(getIdeasError(err));
  }
}

//Get new Idea Saga
export function* getNewIdeaSaga() {
  try {
    const response = yield call(request, "/ideas/new", {
      headers: {
        Accept: "application/json",
      },
    });
    yield put(getNewIdeaSuccess(response));
  } catch (err) {
    yield put(getNewIdeaError(err));
  }
}

//update idea saga
export function* updateIdeaSaga({ idea }) {
  try {
    yield call(request, "/idea/update", {
      method: "POST",
      body: JSON.stringify(idea),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
    yield put(updateIdeaSuccess(idea));
  } catch (err) {
    yield put(updateIdeaError(err));
  }
}

//delete idea saga
export function* deleteIdeaSaga({ id }) {
  try {
    yield call(request, "/idea/delete", {
      method: "POST",
      body: JSON.stringify(id),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
    yield put(deleteIdeaSuccess(id));
  } catch (err) {
    yield put(deleteIdeaError(err));
  }
}

export default function* ideasWatcher() {
  yield takeLatest(GET_IDEAS, getIdeasSaga);
  yield takeLatest(GET_NEW_IDEA, getNewIdeaSaga);
  yield takeLatest(UPDATE_IDEA, updateIdeaSaga);
  yield takeLatest(DELETE_IDEA, deleteIdeaSaga);
}
