// GET All Ideas
export const GET_IDEAS = "GET_IDEAS";
export const GET_IDEAS_SUCCESS = "GET_IDEAS_SUCCESS";
export const GET_IDEAS_ERROR = "GET_IDEAS_ERROR";

//Get new Idea
export const GET_NEW_IDEA = "GET_NEW_IDEA";
export const GET_NEW_IDEA_SUCCESS = "GET_NEW_IDEA_SUCCESS";
export const GET_NEW_IDEA_ERROR = "GET_NEW_IDEA_ERROR";

//Update idea
export const UPDATE_IDEA = "UPDATE_IDEA";
export const UPDATE_IDEA_SUCCESS = "UPDATE_IDEA_SUCCESS";
export const UPDATE_IDEA_ERROR = "UPDATE_IDEA_ERROR";

//Delete idea
export const DELETE_IDEA = "DELETE_IDEA";
export const DELETE_IDEA_SUCCESS = "DELETE_IDEA_SUCCESS";
export const DELETE_IDEA_ERROR = "DELETE_IDEA_ERROR";
