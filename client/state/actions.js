import {
  GET_IDEAS,
  GET_IDEAS_SUCCESS,
  GET_IDEAS_ERROR,
  GET_NEW_IDEA,
  GET_NEW_IDEA_SUCCESS,
  GET_NEW_IDEA_ERROR,
  UPDATE_IDEA,
  UPDATE_IDEA_SUCCESS,
  UPDATE_IDEA_ERROR,
  DELETE_IDEA,
  DELETE_IDEA_SUCCESS,
  DELETE_IDEA_ERROR,
} from "./constants";

// get all ideas
export function getIdeas() {
  return {
    type: GET_IDEAS,
  };
}

export function getIdeasSuccess(ideas) {
  return {
    type: GET_IDEAS_SUCCESS,
    ideas,
  };
}

export function getIdeasError() {
  return {
    type: GET_IDEAS_ERROR,
  };
}

// Get new idea
export function getNewIdea() {
  return {
    type: GET_NEW_IDEA,
  };
}

export function getNewIdeaSuccess(idea) {
  return {
    type: GET_NEW_IDEA_SUCCESS,
    idea,
  };
}

export function getNewIdeaError() {
  return {
    type: GET_NEW_IDEA_ERROR,
  };
}

//update idea
export function updateIdea(idea) {
  return {
    type: UPDATE_IDEA,
    idea,
  };
}

export function updateIdeaSuccess(idea) {
  return {
    type: UPDATE_IDEA_SUCCESS,
    idea,
  };
}

export function updateIdeaError() {
  return {
    type: UPDATE_IDEA_ERROR,
  };
}

//delete idea
export function deleteIdea(id) {
  return {
    type: DELETE_IDEA,
    id,
  };
}

export function deleteIdeaSuccess(id) {
  return {
    type: DELETE_IDEA_SUCCESS,
    id,
  };
}

export function deleteIdeaError() {
  return {
    type: DELETE_IDEA_ERROR,
  };
}
