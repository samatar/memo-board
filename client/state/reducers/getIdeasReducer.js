import { GET_IDEAS, GET_IDEAS_SUCCESS, GET_IDEAS_ERROR } from "../constants";

function rootReducer(state, action) {
  switch (action.type) {
    case GET_IDEAS:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case GET_IDEAS_SUCCESS:
      return {
        ...state,
        ideas: action.ideas,
        loading: false,
        error: false,
      };
    case GET_IDEAS_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
      };
    default:
      return state;
  }
}

export default rootReducer;
