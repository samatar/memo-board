import reduceReducers from "reduce-reducers";
import getIdeasReducer from "./getIdeasReducer";
import getNewIdeaReducer from "./getNewIdeaREducer";
import updateIdeaReducer from "./updateIdeaReducer";
import deleteIdeaReducer from "./deleteIdeaReducer";

const initialState = {
  ideas: [],
  loading: false,
  error: false,
};

const reducer = reduceReducers(
  initialState,
  getIdeasReducer,
  getNewIdeaReducer,
  updateIdeaReducer,
  deleteIdeaReducer
);

export default reducer;
