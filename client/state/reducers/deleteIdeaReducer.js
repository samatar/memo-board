import {
  DELETE_IDEA,
  DELETE_IDEA_SUCCESS,
  DELETE_IDEA_ERROR,
} from "../constants";

function rootReducer(state, action) {
  switch (action.type) {
    case DELETE_IDEA:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case DELETE_IDEA_SUCCESS:
      return {
        ...state,
        ideas: state.ideas.filter(idea => idea.id !== action.id),
        loading: false,
        error: false,
      };
    case DELETE_IDEA_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
      };

    default:
      return state;
  }
}

export default rootReducer;
