import {
  GET_NEW_IDEA,
  GET_NEW_IDEA_SUCCESS,
  GET_NEW_IDEA_ERROR,
} from "../constants";

function rootReducer(state, action) {
  switch (action.type) {
    case GET_NEW_IDEA:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case GET_NEW_IDEA_SUCCESS:
      return {
        ...state,
        ideas: [
          ...state.ideas,
          {
            id: action.idea.id,
            created_date: action.idea.created_date,
            title: "",
            body: "",
          },
        ],
        loading: false,
        error: false,
      };
    case GET_NEW_IDEA_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
      };

    default:
      return state;
  }
}

export default rootReducer;
