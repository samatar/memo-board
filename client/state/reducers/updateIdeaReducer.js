import {
  UPDATE_IDEA,
  UPDATE_IDEA_SUCCESS,
  UPDATE_IDEA_ERROR,
} from "../constants";

function rootReducer(state, action) {
  switch (action.type) {
    case UPDATE_IDEA:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case UPDATE_IDEA_SUCCESS:
      return {
        ...state,
        ideas: state.ideas.map(idea => {
          if (idea.id === action.idea.id)
            return {
              ...idea,
              title: action.idea.title,
              body: action.idea.body,
            };
          else return idea;
        }),
        loading: false,
        error: false,
      };
    case UPDATE_IDEA_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
      };

    default:
      return state;
  }
}

export default rootReducer;
