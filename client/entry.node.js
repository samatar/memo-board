import React from "react";
import { renderToString } from "react-dom/server";
import { ServerStyleSheet, StyleSheetManager } from "styled-components";
import { Provider } from "react-redux";
import App from "./app";
import configureStore from "./state/store";

export default state => {
  let appString, styleTags;
  const sheet = new ServerStyleSheet();

  try {
    appString = renderToString(
      <Provider store={configureStore(state)}>
        <StyleSheetManager sheet={sheet.instance}>
          <App />
        </StyleSheetManager>
      </Provider>
    );

    styleTags = sheet.getStyleTags();
  } catch (error) {
    console.log(error);
  } finally {
    sheet.seal();
  }

  return {
    appString,
    styleTags,
  };
};
