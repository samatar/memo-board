For accessibility, I used a list item to group the ideas. This is because screen readers provide shortcuts to lists and between list items. Screen readers also enumerate the items so users know how many ideas are available.

I also used labels for the input fields, so that screen reader users are able to know what input it required of them.
