const uuid = require("uuid/v4");

module.exports = (req, res) => {
  res.send({
    id: uuid(),
    created_date: new Date(),
  });
};
