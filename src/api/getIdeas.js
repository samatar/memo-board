const ideas = require("../constants/ideas");

module.exports = (req, res) => {
  res.send(ideas);
};
