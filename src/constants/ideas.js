const uuid = require("uuid/v4");
const faker = require("faker");

const ideas = new Array(40).fill().map(() => ({
  id: uuid(),
  created_date: new Date(),
  title: `${faker.name.firstName()} ${faker.name.lastName()}`,
  body: faker.lorem.text(10).slice(0, 140),
}));

module.exports = ideas;
