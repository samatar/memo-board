const express = require("express");
const app = express();

app.disable("x-powered-by");

//Middleware
const render = require("./middleware/render");
const morgan = require("morgan");

//Api
const getIdeas = require("./api/getIdeas");
const getNewIdea = require("./api/getNewIdea");
const updateIdea = require("./api/updateIdea");
const deleteIdea = require("./api/deleteIdea");

if (process.env.NODE_ENV !== "production" && process.env.NODE_ENV !== "test") {
  //Serve hot-reloading bundle to client
  require("./middleware/hmr")(app);
}

app.use(morgan("combined"));
app.use("/assets", express.static("assets"));
app.get("/", render);

app.get("/ideas", getIdeas);
app.get("/ideas/new", getNewIdea);
app.post("/idea/update", updateIdea);
app.post("/idea/delete", deleteIdea);

module.exports = app;
