/**
 * @jest-environment node
 */

const request = require("supertest");
const app = require("../app");

describe("Test the root path", () => {
  it("should respond with a statusCode of 200", async () => {
    const result = await request(app).get("/");
    expect(result.status).toEqual(200);
  });

  it("Should respond with a content-type of text/html", async () => {
    const result = await request(app).get("/");
    expect(result.type).toEqual("text/html");
  });
});

describe("#getIdeas", () => {
  it("should return a list of ideas", async () => {
    const result = await request(app).get("/ideas");
    expect(result.status).toEqual(200);
  });

  it("should respond with a content-type of application/json", async () => {
    const result = await request(app).get("/ideas");
    expect(result.type).toEqual("application/json");
  });

  it("should respond with 40 ideas containing an id, created_date, title and body field", async () => {
    const result = await request(app).get("/ideas");
    const idea = result.body[0];
    expect(result.body.length).toEqual(40);
    expect(idea).toHaveProperty("id");
    expect(idea).toHaveProperty("created_date");
    expect(idea).toHaveProperty("title");
    expect(idea).toHaveProperty("body");
  });
});
