const CDN_URL = process.env.CDN_URL || "/assets";
const ideas = require("../constants/ideas");

module.exports = async (req, res) => {
  let bundle;
  try {
    bundle = require("../../bundles/main").default;
  } catch (err) {
    throw new Error(err, "bundle not found!");
  }

  const appState = { ideas };
  const { appString, styleTags } = bundle(appState);

  const html = `<!DOCTYPE html>
  <html lang="en">
  <head>
    <link rel="preload" href="${CDN_URL}/js/main.js" as="script">
    ${styleTags}
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Memo Board</title>
  </head>
  <body>
    
    <div id="root">${appString}</div> 

    <!-- APP_STATE -->
    <script>window.__STATE__ = ${JSON.stringify(appState)}</script>
    <script src="${CDN_URL}/js/main.js"></script>
    </body>
  </html>`;

  res.send(html);
};
