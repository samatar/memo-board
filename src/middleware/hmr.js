const webpack = require("webpack");
const webpackConfig = require("../../config/build/browser.conf");
const serverConfig = require("../../config/build/node.conf");
const compiler = webpack(webpackConfig);
const serverCompiler = webpack(serverConfig);

function hmr(app) {
  app.use(
    require("webpack-dev-middleware")(compiler, {
      noInfo: true,
      publicPath: webpackConfig.output.publicPath,
    })
  );
  app.use(
    require("webpack-dev-middleware")(serverCompiler, {
      noInfo: true,
      writeToDisk: true,
      lazy: true,
    })
  );
  app.use(require("webpack-hot-middleware")(compiler));
  app.use(require("webpack-hot-middleware")(serverCompiler));

  serverCompiler.plugin("done", function() {
    Object.keys(require.cache).forEach(function(id) {
      delete require.cache[id];
    });
  });
}

module.exports = hmr;
